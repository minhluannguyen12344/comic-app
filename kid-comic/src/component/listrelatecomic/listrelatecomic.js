import React from "react";
import { Box, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
const Listrelatecomic = (props) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "start",
        alignItems: "start",
        marginTop: "0.5rem",
        cursor: "pointer",
        "&:hover": {
          backgroundColor: grey,
          opacity: 0.6,
        },
      }}
    >
      <img
        alt=""
        src={`${props.src}`}
        style={{
          width: "3rem",
          height: "5rem",
          borderRadius: "0.5rem",
        }}
      />
      <Box sx={{ fontSize: "1rem", marginLeft: "0.5rem" }}>
        <Typography sx={{ fontWeight: "bold" }}>{props.title}</Typography>
        <Typography>{props.day}</Typography>
      </Box>
    </Box>
  );
};
export default Listrelatecomic;
