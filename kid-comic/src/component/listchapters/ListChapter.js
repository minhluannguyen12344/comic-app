import React from "react";
import { Box } from "@mui/material";
const Listchapter = (props) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        fontSize: "1rem",
        alignItems: "center",
        backgroundColor: `${props.backgroundColor}`,
        gap: "1rem",
        cursor: "pointer",
        "&:hover": {
          backgroundColor: `${props.backgroundColor}`,
          opacity: 0.7,
        },
      }}
    >
      <Box
        sx={{
          width: "0.2rem",
          height: "4.5rem",
          backgroundColor: "gray",
          marginRight: "1px",
        }}
      ></Box>
      <Box sx={{ fontWeight: "bold" }}>Chương {props.chuong}</Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          my: "1rem",
        }}
      >
        <Box sx={{ color: "gray" }}>{props.chaptername}</Box>
        <Box sx={{ color: "gray" }}>
          {props.day}
          {"  "} ngày trước - {props.view} {"  "}k lượt xem - {props.comment}
          {"  "}bình luận
        </Box>
      </Box>
    </Box>
  );
};
export default Listchapter;
