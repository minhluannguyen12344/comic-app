import React, { useState } from "react";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import { Button } from "@mui/material";
import { useEffect } from "react";
import { useScrollTrigger, Zoom } from "@mui/material";
const ScrollToTopButton = () => {
  const [isVisible, setIsVisible] = useState(false);

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  useEffect(() => {
    setIsVisible(trigger);
  }, [trigger]);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <Zoom in={isVisible}>
      <Button
        sx={{
          backgroundColor: "gray",
          position: "fixed",
          bottom: "1rem",
          left: "1rem",
        }}
        onClick={scrollToTop}
      >
        <ArrowUpwardIcon />
      </Button>
    </Zoom>
  );
};

export default ScrollToTopButton;
