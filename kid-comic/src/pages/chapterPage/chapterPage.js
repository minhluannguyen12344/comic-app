import React from "react";
import { Box } from "@mui/material";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import Grid from "@mui/material/Grid";
import NavBar from "../../views/Navbar/Navbar";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Link } from "@mui/material";
import Listchapter from "../../component/listchapters/ListChapter";
import Footer from "../../views/Footer/Footer";
import Listrelatecomic from "../../component/listrelatecomic/listrelatecomic";
const ChapterPage = () => {
  return (
    <Box
      sx={{
        backgroundColor: "#e0e0e0",
        display: "flex",
        height: "auto",
        flexDirection: "column",
        boxSizing: "border-box",
        justifyContent: "center",
        alignItems: "center",
        width: 1,
        fontSize: "1.5rem",
      }}
    >
      <NavBar />
      <Box
        sx={{
          width: "80%",
          margin: "8rem 5rem 0 5rem",
          marginBottom: "1.5rem",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: 1,
            backgroundColor: "#D8D9DA",
          }}
        >
          <Box sx={{ position: "relative" }}>
            <img
              style={{
                width: "100%",
                height: "600px",
                borderRadius: "0.5rem 0.5rem 0 0",
                objectFit: "cover",
              }}
              src="https://ecdn.game4v.com/g4v-content/uploads/2022/07/21101835/Black-Clover-1-game4v-1658373512-15.png"
              alt=""
            />
            <Box
              sx={{
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: "100%",
                zIndex: 2,
                borderRadius: "0.5rem 0.5rem 0 0",
                background:
                  "linear-gradient(0deg, #000, rgba(0,0,0,.959) 5.79%, rgba(0,0,0,.891) 10.88%, rgba(0,0,0,.802) 15.63%, rgba(0,0,0,.696) 20.37%, rgba(0,0,0,.581) 25.46%, rgba(0,0,0,.463) 31.25%, rgba(0,0,0,.346) 38.08%, rgba(0,0,0,.237) 46.3%, rgba(0,0,0,.142) 56.25%, rgba(0,0,0,.067) 68.29%, rgba(0,0,0,.018) 82.75%, #0000)",
              }}
            ></Box>
            <Box
              sx={{
                position: "absolute",
                bottom: "-30%",
                left: "10%",
                zIndex: 3,
              }}
            >
              <img
                style={{
                  width: "14rem",
                  height: "21rem",
                  borderRadius: "0.5rem",
                  objectFit: "cover",
                }}
                src="https://images.fineartamerica.com/images/artworkimages/mediumlarge/3/1-asta-black-clover-noah-dupont.jpg"
                alt=""
              />
            </Box>
            <Box
              sx={{
                color: "white",
                position: "absolute",
                marginLeft: "25rem",
                bottom: "5%",
                zIndex: 3,
              }}
            >
              <Box>Tabata Yuuki</Box>
              <Box sx={{ fontWeight: "bold" }}>Black Clover (FULL HD)</Box>
            </Box>
          </Box>
          <Box
            sx={{
              marginLeft: "25rem",
              width: "50%",
              paddingBottom: "1rem",
              marginRight: "3rem",
            }}
          >
            <Box
              sx={{
                display: "flex",
                marginTop: "1rem",
                alignItems: "center",
              }}
            >
              <AccessTimeIcon />
              <Typography sx={{ fontSize: "1rem" }}>7 ngày trước</Typography>
            </Box>
            <Box
              sx={{
                width: 1,
                height: "5rem",
                margin: "0.5rem 0 0 0",
                display: "flex",
                flexWrap: "wrap",
                gap: "0.5rem",
                paddingBottom: "0.5rem",
              }}
            >
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Manga
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Fantasy
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Shounen
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Hành động
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Phép thuật
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Siêu nhiên
              </Box>
              <Box
                sx={{
                  borderRadius: "50px",
                  backgroundColor: "white",
                  width: "fit-content",
                  height: "1.5rem",
                  padding: "0.5rem 1rem 0.5rem 1rem",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: "black",
                  opacity: "0.6",
                }}
              >
                Drama
              </Box>
            </Box>
            <Box sx={{ display: "flex", gap: "1rem" }}>
              <Button variant="contained" disabled>
                Đăng Nhập Để Theo Dõi
              </Button>
              <Button variant="contained">Đọc từ chương 1</Button>
            </Box>
          </Box>
        </Box>
        <Box sx={{ backgroundColor: "white" }}>
          <Grid container>
            <Grid item md={9} sx={{ padding: "1rem" }}>
              <Box
                sx={{
                  backgroundColor: "#F5F5F5",

                  borderRadius: "0.5rem",
                  paddingTop: "0.5rem",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    margin: "1rem",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Box sx={{ display: "flex", flexDirection: "row" }}>
                      <Avatar
                        alt="Remy Sharp"
                        src="https://khoinguonsangtao.vn/wp-content/uploads/2022/07/avatar-hai-1.jpg"
                      />
                      <Box sx={{ fontSize: "1rem", marginLeft: "1rem" }}>
                        <Box sx={{ fontWeight: "bold" }}>HaoLun</Box>
                        <Box>69 Truyện</Box>
                      </Box>
                    </Box>
                    <Link
                      sx={{
                        color: "gray",
                        textDecoration: "none",
                        fontSize: "1rem",
                        "&:hover": {
                          color: "green",
                        },
                      }}
                      href="https://www.facebook.com/vuong.g.hao"
                    >
                      https://www.facebook.com/vuong.g.hao
                    </Link>
                  </Box>
                  <Box sx={{ fontSize: "1rem", marginTop: "1rem" }}>
                    Asta và Yuno là hai đứa trẻ bị bỏ rơi ở nhà thờ và cùng nhau
                    lớn lên tại đó. Bấy giờ, hai người bạn trẻ đang hướng ra thế
                    giới, cùng chung mục tiêu.
                  </Box>
                </Box>
                <Box
                  sx={{
                    backgroundColor: "#D8D9DA",
                    height: "auto",
                  }}
                >
                  <Box
                    sx={{
                      margin: "1rem",
                      fontSize: "1rem",
                      display: "flex",
                      justifyContent: "space-between",
                      gap: "1rem",
                      flexDirection: "column",
                    }}
                  >
                    <Typography
                      sx={{
                        fontWeight: "bold",
                        color: "#61677A",
                        marginTop: "0.5rem",
                      }}
                    >
                      Trang chính thức
                    </Typography>
                    <Link href="https://nxbkimdong.com.vn/">
                      nxbkimdong.com.vn
                    </Link>
                    <Typography sx={{ fontWeight: "bold", color: "#61677A" }}>
                      Thông tin thêm
                    </Typography>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        gap: "0.5rem",
                      }}
                    >
                      <Typography sx={{ fontWeight: "bold" }}>89</Typography>
                      <Typography>Chương đã đăng</Typography>
                    </Box>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: "0.5rem",
                        gap: "0.5rem",
                      }}
                    >
                      <Typography sx={{ fontWeight: "bold" }}>1.2M</Typography>
                      <Typography>Lượt xem</Typography>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  height: "50rem",
                  width: 1,
                  overflowY: "scroll",
                  display: "flex",
                  flexDirection: "column",
                  gap: "0.3rem",
                }}
              >
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#F5F5F5"
                />
                <Listchapter
                  chuong="366"
                  chaptername="Ngôi sao chính"
                  day="7"
                  view="10.7"
                  comment="44"
                  backgroundColor="#EEEEEE"
                />
              </Box>
            </Grid>
            <Grid item md={3}>
              <Box sx={{ margin: "1rem", padding: "1rem" }}>
                <Box sx={{ fontSize: "1rem", fontWeight: "bold" }}>Đề xuất</Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    flexWrap: "wrap",
                  }}
                >
                  <Listrelatecomic
                    src="https://images.spiderum.com/sp-images/4f477470e67011ea8560d1cc372e8d47.jpg"
                    title="My Hero Academia (FULL HD)"
                    day="C. 395 - 12 NGÀY TRƯỚC"
                  />
                  <Listrelatecomic
                    src="https://cuunews.net/king-include/uploads/2022/11/683897-shinjxu7ptm91.jpg"
                    title="Thất Nghiệp Chuyển Sinh (FULL HD)"
                    day="C. 90 - 2 THÁNG TRƯỚC"
                  />
                  <Listrelatecomic
                    src="https://m.media-amazon.com/images/M/MV5BYmU2MzEyMjAtOTQ5Yy00NGMxLTg0NmItMTQ0ZTM5OGY0NjUzXkEyXkFqcGdeQXVyNjAwNDUxODI@._V1_FMjpg_UX1000_.jpg"
                    title="Dr.Stone"
                    day="C. 233 - 1 NĂM TRƯỚC"
                  />
                  <Listrelatecomic
                    src="https://m.media-amazon.com/images/M/MV5BZmQ5NGFiNWEtMmMyMC00MDdiLTg4YjktOGY5Yzc2MDUxMTE1XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_FMjpg_UX1000_.jpg"
                    title="NARUTO (FULL HD)"
                    day="C. Cuối - 2 NĂM TRƯỚC"
                  />
                  <Listrelatecomic
                    src="https://m.media-amazon.com/images/M/MV5BODFmYTUwYzMtM2M2My00NGExLWIzMDctYmRjNTNhZDc4MGI2XkEyXkFqcGdeQXVyMTMzNDExODE5._V1_.jpg"
                    title="BLEACH (FULL HD)"
                    day="C. 456 - 5 NGÀY TRƯỚC"
                  />
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default ChapterPage;
