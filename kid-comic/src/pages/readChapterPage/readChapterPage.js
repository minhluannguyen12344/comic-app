import React from "react";
import { Box, Typography, Button } from "@mui/material";
import NavBar from "../../views/Navbar/Navbar";
import Footer from "../../views/Footer/Footer";
import ScrollToTopButton from "../../component/btnScrollTop/btnScrollTop";
const ReadPage = () => {
  let img1 = require("../../assets/img/img1.png");
  let img2 = require("../../assets/img/img2.png");
  let img3 = require("../../assets/img/img3.png");
  let img4 = require("../../assets/img/img4.png");
  let img5 = require("../../assets/img/img5.png");
  let img6 = require("../../assets/img/img6.png");
  let img7 = require("../../assets/img/img7.png");
  let img8 = require("../../assets/img/img8.png");
  let img9 = require("../../assets/img/img9.png");
  let img10 = require("../../assets/img/img10.png");
  let img11 = require("../../assets/img/img11.png");
  let img12 = require("../../assets/img/img12.png");
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "rgba(17, 24, 30)",
      }}
    >
      <NavBar />
      <ScrollToTopButton />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          marginTop: "6rem",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            gap: "0.5rem",
            width: "100%",
            alignItems: "center",
          }}
        >
          <Typography
            sx={{ color: "white", fontSize: "2.5rem", fontWeight: "bold" }}
          >
            {" "}
            Chương 366 : Ngôi Sao Chính
          </Typography>
          <Button
            sx={{
              width: "40rem",
              backgroundColor: "gray",
              color: "white",
            }}
          >
            Chương Sau
          </Button>
          <Button
            variant="contained"
            sx={{
              width: "40rem",
            }}
          >
            Chương trước
          </Button>
        </Box>
        <Box
          sx={{
            marginTop: "1rem",
            display: "flex",
            flexDirection: "column",
            gap: "1.5rem",
            marginBottom: "1rem",
          }}
        >
          <img alt="" src={img1} style={{ width: "60rem" }} />
          <img alt="" src={img2} style={{ width: "60rem" }} />
          <img alt="" src={img3} style={{ width: "60rem" }} />
          <img alt="" src={img4} style={{ width: "60rem" }} />
          <img alt="" src={img5} style={{ width: "60rem" }} />
          <img alt="" src={img6} style={{ width: "60rem" }} />
          <img alt="" src={img7} style={{ width: "60rem" }} />
          <img alt="" src={img8} style={{ width: "60rem" }} />
          <img alt="" src={img9} style={{ width: "60rem" }} />
          <img alt="" src={img10} style={{ width: "60rem" }} />
          <img alt="" src={img11} style={{ width: "60rem" }} />
          <img alt="" src={img12} style={{ width: "60rem" }} />
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default ReadPage;
