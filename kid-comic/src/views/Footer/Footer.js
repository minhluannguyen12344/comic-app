import React from "react";

const Footer = () => {
    return(
        <footer className="footer">
            <div className="footerbody">
                <div className="footerslogan"> 
                    Created by VuTran and LuanNguyen - 2023
                </div>
            </div>
        </footer>
    )
}
export default Footer;