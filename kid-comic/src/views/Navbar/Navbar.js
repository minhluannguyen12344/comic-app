import React from "react";
import SearchIcon from '@mui/icons-material/Search';
const NavBar=() => {
    return(
        <nav className="navbar">
            <div className="navattribute">
               <div className="boxnavbar">
                    <div className="leftnav">
                        <div className="slogan">
                            Kho truyện tranh cho học sinh tiểu học
                        </div>
                    </div>
                    <div className="middlenav">
                        <a href="/*" className="name">Gác Văn</a>
                    </div>
                    <div className="endnav">
                        <div>
                            <button className="loginbutton">
                                Đăng nhập
                            </button>
                        </div>
                        <div>
                            <button className="loginbutton">
                                Đăng ký
                            </button>
                        </div>
                        <div>
                            <button className="searchbutton">
                                <span>
                                    <SearchIcon/>    
                                </span>    
                            </button>
                        </div>
                    </div>
               </div>
            </div>
        </nav>
    )
}
export default NavBar;