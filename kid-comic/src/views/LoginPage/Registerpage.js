import React from "react";
import NavBar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";
const RegisterPage = () => {
    return(
        <div>
            <NavBar />
            <div className="loginbody">
                <div className="loginform">
                    <div className="loginbox">
                        <div className="formname">
                            <div className="formtitle">đăng ký tài khoản</div>
                        </div>
                        <form>
                            <div className="mb">
                                <label for="username" className="label">
                                    Tên đăng nhập
                                </label>
                                <input id="username" type="text" placeholder="Tên đăng nhập" className="input" />
                            </div>
                            <div className="mb">
                                <label for="password" className="label">
                                    Mật khẩu
                                </label>
                                <input id="pwd" type="password" placeholder="Nhập mật khẩu" className="input" />
                            </div>
                            <div className="mb">
                                <label for="password" className="label">
                                    Nhập lại mật khẩu
                                </label>
                                <input id="pwd" type="password" placeholder="Nhập lại mật khẩu" className="input" />
                            </div>
                            <div className="mb">
                                <label for="email" className="label">
                                    Email
                                </label>
                                <input id="pwd" type="email" placeholder="Nhập email" className="input" />
                            </div>
                            <div className="mb2">
                                <input id="rememberme" type="checkbox" className="input2" />
                                <label for="password" className="label">
                                    Tôi đồng ý với các điều khoản của trang web
                                </label>
                            </div>
                            <div className="loginbutton2">
                                <button className="logbutton">
                                    Đăng nhập
                                </button>
                            </div>
                            <div className="logfoot">
                                <a href="/login" className="lofootcontent">
                                    Đăng nhập
                                </a>
                                <a href="/forgetpwd" className="lofootcontent">
                                    Quên mật khẩu?
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
export default RegisterPage;