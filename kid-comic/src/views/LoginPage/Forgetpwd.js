import React from "react";
import NavBar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";
const Forgetpwd = () => {
    return(
        <div>
            <NavBar />
            <div className="loginbody">
                <div className="loginform">
                    <div className="loginbox">
                        <div className="formname">
                            <div className="formtitle">Khôi phục mật khẩu</div>
                        </div>
                        <form>
                            <div className="mb">
                                <label for="username" className="label">
                                    Tên đăng nhập
                                </label>
                                <input id="username" type="text" placeholder="Tên đăng nhập" className="input" />
                            </div>
                            <div className="mb">
                                <label for="password" className="label">
                                    Nhập email đăng kí tài khoản
                                </label>
                                <input id="email" type="email" placeholder="Nhập email" className="input" />
                            </div>
                            <div className="loginbutton2">
                                <button className="logbutton">
                                   Khôi phục mật khẩu
                                </button>
                            </div>
                            <div className="logfoot">
                                <a href="/login" className="lofootcontent">
                                    Đăng nhập
                                </a>
                                <a href="/register" className="lofootcontent">
                                    Đăng ký
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        <Footer />
    </div>
    )
}
export default Forgetpwd;