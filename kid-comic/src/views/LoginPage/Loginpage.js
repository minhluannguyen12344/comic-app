import React from "react";
import NavBar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";
const LoginPage = () => {
    return(
        <div>
            <NavBar />
            <div className="loginbody">
                <div className="loginform">
                    <div className="loginbox">
                        <div className="formname">
                            <div className="formtitle">Đăng nhập</div>
                        </div>
                        <form>
                            <div className="mb">
                                <label for="username" className="label">
                                    Tên đăng nhập
                                </label>
                                <input id="username" type="text" placeholder="Tên đăng nhập" className="input" />
                            </div>
                            <div className="mb">
                                <label for="password" className="label">
                                    Mật khẩu
                                </label>
                                <input id="pwd" type="password" placeholder="Mật khẩu" className="input" />
                            </div>
                            <div className="mb2">
                                <input id="rememberme" type="checkbox" className="input2" />
                                <label for="password" className="label">
                                    Ghi nhớ mật khẩu
                                </label>
                            </div>
                            <div className="loginbutton2">
                                <button className="logbutton">
                                    Đăng nhập
                                </button>
                            </div>
                            <div className="logfoot">
                                <a href="/forgetpwd" className="lofootcontent">
                                    Quên mật khẩu?
                                </a>
                                <a href="/register" className="lofootcontent">
                                    Đăng ký
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        <Footer />
    </div>
    )
}
export default LoginPage;