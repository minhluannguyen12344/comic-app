import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./views/LoginPage/Loginpage";
import RegisterPage from "./views/LoginPage/Registerpage";
import { Forgetpwd } from "./views/LoginPage";
import "./index.css";
import ChapterPage from "./pages/chapterPage/chapterPage";
import ReadPage from "./pages/readChapterPage/readChapterPage";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<LoginPage />}></Route>
        <Route path="/register" element={<RegisterPage />}></Route>
        <Route path="/forgetpwd" element={<Forgetpwd />}></Route>
        <Route path="/chapter" element={<ChapterPage />}></Route>
        <Route path="/chapter/chapter_id" element={<ReadPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
